var http = require('http');
var url = require('url');

// module.exports is a middleware
module.exports = function (redirects, port, ip) {
    var redirect = function(req, res, redirect){
            if (redirect){
              var newUrl = redirect.host + url.parse(req.url).pathname;

              res.statusCode = redirect.code || 302;
              res.setHeader('Content-Type', 'text/plain');
              res.setHeader('Location', newUrl);
              res.end('Redirecting to '+newUrl);
            } else {
                // there is no catch all, we will just show an error message
                res.statusCode = 404;
                res.setHeader('Content-Type', 'text/plain');
                res.end('Host not found');
            }
        },

        server =  http.createServer(function (req, res) {
            var domain;
            
            http.get('http://www1.rioclaro.somasig.com.br:8002/', function (response) {
                domain = redirects['www1'];
                console.log('GVT is working');
                redirect(req, res, domain);

            }).on('error', function(e) { // GVT link not working
                console.log('GVT is not working');
                domain = redirects['www2'];
                redirect(req, res, domain);

            });    
        }).listen(port, ip);

  return server;
}
